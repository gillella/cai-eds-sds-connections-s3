package com.coxautoinc.eds.sds.common.connections;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.coxautoinc.eds.common.DefaultValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by akgillella on 7/25/16.
 */
public class S3Connection {

    private static final Logger logger = LoggerFactory.getLogger(S3Connection.class);

    @Inject
    @DefaultValue(key= "s3.accessKey", value= "")
    private  String s3AccessKey;

    @Inject
    @DefaultValue(key = "s3.secretKey", value = "")
    private  String s3SecretKey;

    @Inject
    @DefaultValue(key = "s3.connection.timeout", value = "5000")
    private  int s3ConnectionTimeout;

    @Inject
    @DefaultValue(key = "s3.connections.maxpool", value = "10")
    private  int s3MaxPool;

    private AmazonS3 s3con;

    /**
     * Default private constructor
     */
    @PostConstruct
    public void init(){

        logger.info("Initializing S3 connection for archive Store");

        AWSCredentials credentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey);
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setConnectionTimeout(s3ConnectionTimeout);
        clientConfig.setMaxConnections(s3ConnectionTimeout);


        try{
            s3con = new AmazonS3Client(credentials, clientConfig);
        }catch(Exception e){
            logger.info("Error initializing S3 connection for archive Store", e);
            return;
        }

        logger.info("Completed Initializing S3 connection for archive Store : {}", s3con);

    }


    /**
     * Get S3 connection
     *
     * @return AmazonS3
     */
    public AmazonS3 get() {
        return s3con;
    }

}
